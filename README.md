# UCBL-BACKGROUND-SUBSTRACTION PACKAGE
In this package, you will find:

The algorithm algorithm.py file contains 2 algorithm of background substraction : MOG2 and KNN

The main.py file is the entry point of the program. It parses command arguments, and [load => filter => write] a video file accordingly.

Accordingly to this package, we provides some usage of this package :
`python3 -m ucbl-background-extraction -i path/to/input.mp4 -o path/to/output.mp4 "KNN()"`
We can have anothers examples:
`python3 -m ucbl-background-extraction -i path/to/input.mp4 -o path/to/output.mp4 "MOG2()"`



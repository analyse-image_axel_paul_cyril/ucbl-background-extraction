import cv2


class MOG2(object):
    def __init__(self):
        self.backSub = cv2.createBackgroundSubtractorMOG2()

    def apply(self, frame):
        return self.backSub.apply(frame)


class KNN(object):
    def __init__(self):
        self.backSub = cv2.createBackgroundSubtractorKNN()

    def apply(self, frame):
        return self.backSub.apply(frame)


def factory(definition):
    return eval(definition)

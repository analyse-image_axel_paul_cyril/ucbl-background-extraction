import sys, getopt
import cv2

from ucbl_video_io import VideoReader, VideoWriter
from ucbl_background_extraction.Algorithms import factory


def usage():
    print("Usage: \n -i, --input    select a file as input (image or video). \n -o, --output\
    the recipient file.\n -d        Debug mod.\n And, specified the algorithm you want in quotation marks. \n Algorithm : \n\
      MOG2()\n      KNN()\n\n\
example : python3 -m ucbl-background-extraction -i myVideo.mp4 -o output.mp4 \"MOG2()\"" )
    


def read_args():
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hg:i:o:d", ["help", "input=", "output="])
    except getopt.GetoptError:
        usage()
        sys.exit(2)

    input_file_arg = None
    output_file_arg = None

    for op, arg in opts:
        if op in ("-h", "--help"):
            usage()
            sys.exit()
        elif op == '-d':
            global _debug
            _debug = 1
        elif op in ("-i", "--input"):
            input_file_arg = arg
        elif op in ("-o", "--output"):
            output_file_arg = arg

    return input_file_arg, output_file_arg, args


def main():
    if_arg, of_arg, algo_arg = read_args()

    if if_arg is None:
        raise AttributeError("Missing input (-i <input path>)/(--input=<input path>) argument")
    if of_arg is None:
        raise AttributeError("Missing output (-o <output path>)/(--output=<output path>) argument")

    if len(algo_arg) == 0:
        raise AttributeError("Missing background-removal algorithm definition")
    if len(algo_arg) != 1:
        raise AttributeError("Only one background-removal algorithm allowed per run")

    brAlgorithm = factory(algo_arg[0])

    with VideoReader(if_arg) as input_file:
        with VideoWriter(of_arg, input_file.get_meta()) as output_file:
            for frame in input_file.read():
                output_file.write(cv2.cvtColor(brAlgorithm.apply(frame), cv2.COLOR_GRAY2BGR))


if __name__ == "__main__":
    # execute only if run as a script
    main()
